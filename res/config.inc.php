<?php
if (!defined ('ORM_LIB_PATH'))
	define ('ORM_LIB_PATH', ORM_PATH . 'lib' . DIRECTORY_SEPARATOR);

if (!defined ('ORM_RES_PATH'))
	define ('ORM_RES_PATH', ORM_PATH . 'res' . DIRECTORY_SEPARATOR);

if (!defined ('ORM_TEST_PATH'))
	define ('ORM_TEST_PATH', ORM_PATH . 'unit_testing' . DIRECTORY_SEPARATOR);
